import { Routes, Route, Link, Navigate, Outlet } from "react-router-dom";
import React from "react";

import Admin from "./pages/Admin";
import Analitycs from "./pages/Analitycs";
import Dashboard from "./pages/Dashboard";
import Home from "./pages/Home";
import Landing from "./pages/Landing";

const ProtectRoute = ({ isAllowed, redirectPatch = "/landing", children }) => {
  if (!isAllowed) {
    return <Navigate to={redirectPatch} replace />;
  }
  return children ? children : <Outlet />;
};

const Navigation = () => (
  <nav>
    <Link to="/landing">Landing</Link>
    <Link to="/home">Home</Link>
    <Link to="/dashboard">Dashboard</Link>
    <Link to="/analitycs">Analytics</Link>
    <Link to="/admin">Admin</Link>
  </nav>
);

const App = () => {
  const [user, setUser] = React.useState(null);

  const handleLogin = () =>
    setUser({
      id: "1",
      name: "robin",
      permissions: ["analyze"],
      roles: ["admin"],
    });
  const handleLogout = () => setUser(null);

  return (
    <>
      <h1>React Router</h1>

      <Navigation />

      {user ? (
        <button onClick={handleLogout}>Sign Out</button>
      ) : (
        <button onClick={handleLogin}>Sign In</button>
      )}

      <Routes>
        <Route index element={<Landing />} />
        <Route path="landing" element={<Landing />} />

        <Route element={<ProtectRoute isAllowed={!!user} />}>
          <Route path="home" element={<Home />} />
          <Route path="dashboard" element={<Dashboard />} />
        </Route>

        <Route
          path="analitycs"
          element={
            <ProtectRoute
              isAllowed={!!user && user?.permissions?.includes("analyze")}
              redirectPatch="/home"
            >
              <Analitycs />
            </ProtectRoute>
          }
        />
        <Route
          path="admin"
          element={
            <ProtectRoute
              isAllowed={!!user && user?.roles?.includes("admin")}
              redirectPatch="/dashboard"
            >
              <Admin />
            </ProtectRoute>
          }
        />
        <Route path="*" element={<p>There's nothing here: 404!</p>} />
      </Routes>
    </>
  );
};

export default App;
